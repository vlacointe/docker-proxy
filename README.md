Docker proxy
============

This docker-compose file uses [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy) automatic reverse proxy and [alastaircoote/docker-letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/alastaircoote/docker-letsencrypt-nginx-proxy-companion/) letsencrypt container.

## Usage
1. Create the docker network that will handle containers communication

```docker network create networkname```

2. Change the network name in docker-compose.yml

```sed -i s/proxy-networkname/networkname/g docker-compose.yml```

3. Launch docker-compose

```docker-compose up```

## Other containers configuration

### Network configuration 

Make sure the container is in the same network than the proxy by adding theses lines.

```
networks:
  networkname:
    external:
      name: networkname
```

### Nginx proxy binding

Add an environment variable with domain name

```
    environment:
      - VIRTUAL_HOST=example.com
```

### Letsencrypt certificates

Add these line for the domain example.com

```
    environment:
      - LETSENCRYPT_HOST=example.com
      - LETSENCRYPT_EMAIL=contact@example.com
```
